import Vue from "vue";
import Vuex from "vuex";
import Axios from "axios";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    countries: null,
    countriesLoading: false,
    selectedCountryName: "",
    selectedCountry: null
  },
  getters: {
    getSelectedCountryDetails(state) {
      return state.selectedCountry;
    }
  },
  mutations: {
    setCountries(state, payload) {
      state.countries = payload;
    },
    setLoadingStatus(state, value) {
      state.countriesLoading = value;
    },
    setSelectedCountry(state, value) {
      state.selectedCountryName = value;
      let [result] = state.countries.filter(item => item.name === value);
      if (result) {
        state.selectedCountry = result;
      } else {
        state.selectedCountry = null;
      }
    }
  },
  actions: {
    fetchAllCountries(context) {
      context.commit("setLoadingStatus", true);
      Axios.get(`https://restcountries.eu/rest/v2/all`).then(response => {
        if (response.status === 200) {
          context.commit("setCountries", response.data);
        } else {
          context.commit("setCountries", []);
        }
        context.commit("setLoadingStatus", false);
      });
    },
    setSelectedCountry(context, value) {
      if (context.state.countries) {
        context.commit("setSelectedCountry", value);
      } else {
        // a workaround. Better solution use Promise on fetchAllCountries
        context.dispatch("fetchAllCountries");
        setTimeout(() => {
          context.commit("setSelectedCountry", value);
        }, 500);
      }
    }
  },
  modules: {}
});
